import math

def factorial_digit_sum(number):
    factorial_number = math.factorial(number)
    factorial_sum = 0
    for num in str(factorial_number):
        factorial_sum += int(num)
    return factorial_sum

print(factorial_digit_sum(100))